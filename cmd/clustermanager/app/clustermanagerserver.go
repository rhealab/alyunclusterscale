package app

import (
	"encoding/json"
	"fmt"
	"time"

	"net/http"
	"strconv"
	"sync"

	"yunprovider/cmd/clustermanager/app/options"
	"yunprovider/log"

	"github.com/emicklei/go-restful"
	"github.com/golang/glog"
	"k8s.io/apimachinery/pkg/api/errors"

	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apiserver/pkg/authentication/authenticator"
	genericapifilters "k8s.io/apiserver/pkg/endpoints/filters"
	"k8s.io/apiserver/plugin/pkg/authenticator/password/passwordfile"
	"k8s.io/apiserver/plugin/pkg/authenticator/request/basicauth"
	"k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	DynamicConfigMap_ns    = "kube-system"
	DynamicConfigMap_name  = "limit-request-parameter"
	DynamicConfig_itemname = "limit-request-parameter.json"
)

var cmcLock *sync.RWMutex
var d *DynamicConfig
var curCreateMachineType string

type Item struct {
	MaxCreateNode         int
	CreateRequestCpuAbove float64
	CreateLimitCpuAbove   float64
	CreateRequestMemAbove float64
	CreateLimitMemAbove   float64
	DeleteRequestCpuUnder float64
	DeleteLimitCpuUnder   float64
	DeleteRequestMemUnder float64
	DeleteLimitMemUnder   float64
	CreateMachineType     string
}

type DynamicConfig struct {
	Item      *Item
	client    *clientset.Clientset
	configmap *v1.ConfigMap
	cmc       *options.ClusterManagerConfig
}

type ReturnMsg struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data string `json:"data"`
}

func CreateOrUpdateDc(cmc *options.ClusterManagerConfig, client *clientset.Clientset) (*DynamicConfig, error) {

	item := &Item{
		MaxCreateNode:         cmc.MaxCreateNode,
		CreateRequestCpuAbove: cmc.CreateRequestCpuAbove,
		CreateLimitCpuAbove:   cmc.CreateLimitCpuAbove,
		CreateRequestMemAbove: cmc.CreateRequestMemAbove,
		CreateLimitMemAbove:   cmc.CreateLimitMemAbove,
		DeleteRequestCpuUnder: cmc.DeleteRequestCpuUnder,
		DeleteLimitCpuUnder:   cmc.DeleteLimitCpuUnder,
		DeleteRequestMemUnder: cmc.DeleteRequestMemUnder,
		DeleteLimitMemUnder:   cmc.DeleteLimitMemUnder,
		CreateMachineType:     curCreateMachineType,
	}
	buf, err := json.MarshalIndent(item, " ", "  ")
	if err != nil {
		return nil, err
	}

	cm, err := client.CoreV1().ConfigMaps(DynamicConfigMap_ns).Get(DynamicConfigMap_name, meta_v1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			cm = &v1.ConfigMap{
				ObjectMeta: meta_v1.ObjectMeta{
					Namespace: DynamicConfigMap_ns,
					Name:      DynamicConfigMap_name,
				},
				Data: map[string]string{
					DynamicConfig_itemname: string(buf),
				},
			}
			cm, err = client.CoreV1().ConfigMaps(DynamicConfigMap_ns).Create(cm)
		} else {
			log.MyLogE("CreateOrUpdateDc %s:%s , err:%v", DynamicConfigMap_ns, DynamicConfigMap_name, err)
			return nil, err
		}

	} else {
		cm.Data[DynamicConfig_itemname] = string(buf)
		cm, err = client.CoreV1().ConfigMaps(DynamicConfigMap_ns).Update(cm)
	}

	if err != nil {
		return nil, err
	}
	if d == nil {
		d = &DynamicConfig{
			Item:      item,
			client:    client,
			configmap: cm,
			cmc:       cmc,
		}
	} else {
		d.cmc = cmc
		d.Item = item
		d.configmap = cm
		d.client = client
	}

	return d, nil
}

func cmcCopy(cmc *options.ClusterManagerConfig) *options.ClusterManagerConfig {
	tmp, _ := json.Marshal(cmc)
	cmccopy := &options.ClusterManagerConfig{}
	_ = json.Unmarshal(tmp, &cmccopy)
	return cmccopy
}

func (d *DynamicConfig) GetConfigMap() (*v1.ConfigMap, error) {
	if d.configmap != nil {
		return d.configmap, nil
	}

	cm, err := d.client.CoreV1().ConfigMaps(DynamicConfigMap_ns).Get(DynamicConfigMap_name, meta_v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	d.configmap = cm
	return cm, nil
}

func (d *DynamicConfig) tryconfigSave(name, value string) error {

	//update in copy then check it
	cmcLock.RLock()
	cmcCopy := cmcCopy(d.cmc)
	cmcLock.RUnlock()
	var err error

	if name == "MaxCreateNode" {
		value, err := strconv.Atoi(value)
		if err != nil {
			return err
		} else {
			cmcCopy.MaxCreateNode = value
			err := checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.MaxCreateNode = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		}

	} else if name == "CreateMachineType" {
		if err := UpdateCreateMachineType(d.cmc.ProviderUrl, "aliyun", value); err != nil {
			return err
		} else {
			curCreateMachineType = value
			CreateOrUpdateDc(d.cmc, d.client)
			return nil
		}
	} else {
		value, err := strconv.ParseFloat(value, 64)
		if err != nil {
			return err
		}
		switch name {

		case "CreateRequestCpuAbove":
			cmcCopy.CreateRequestCpuAbove = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.CreateRequestCpuAbove = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		case "CreateLimitCpuAbove":
			cmcCopy.CreateLimitCpuAbove = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.CreateLimitCpuAbove = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)

			} else {
				return err
			}
		case "CreateRequestMemAbove":
			cmcCopy.CreateRequestMemAbove = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.CreateRequestMemAbove = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		case "CreateLimitMemAbove":
			cmcCopy.CreateLimitMemAbove = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.CreateLimitMemAbove = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		case "DeleteRequestCpuUnder":
			cmcCopy.DeleteRequestCpuUnder = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.DeleteRequestCpuUnder = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		case "DeleteLimitCpuUnder":
			cmcCopy.DeleteLimitCpuUnder = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.DeleteLimitCpuUnder = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		case "DeleteRequestMemUnder":
			cmcCopy.DeleteRequestMemUnder = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.DeleteRequestMemUnder = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		case "DeleteLimitMemUnder":
			cmcCopy.DeleteLimitMemUnder = value
			err = checkConfig(cmcCopy)
			if err == nil {
				cmcLock.Lock()
				d.cmc.DeleteLimitMemUnder = value
				cmcLock.Unlock()
				CreateOrUpdateDc(d.cmc, d.client)
			} else {
				return err
			}
		default:
			err = fmt.Errorf("set config error:non-standard input")
		}
	}

	return err

}

func (d *DynamicConfig) ClusterManagerConfigGet(request *restful.Request, response *restful.Response) {
	cm, err := d.GetConfigMap()
	if cm == nil {
		response.Write([]byte("undefine configmap"))
		return
	}
	data := cm.Data[DynamicConfig_itemname]
	ret := &Item{}
	err = json.Unmarshal([]byte(data), &ret)
	if err != nil {
		response.Write([]byte("unexpected error"))
		return
	}
	response.WriteAsJson(ret)
}

func (d *DynamicConfig) ClusterManagerConfigSet(request *restful.Request, response *restful.Response) {
	name := request.PathParameter("name")
	value := request.PathParameter("value")
	ret := ReturnMsg{}
	err := d.tryconfigSave(name, value)

	if err != nil {
		ret.Code = 1
		ret.Msg = err.Error()
	} else {
		ret.Code = 0
		ret.Msg = "OK"
	}
	response.WriteAsJson(ret)
}

func StartHttpServer(cmc *options.ClusterManagerConfig, client *clientset.Clientset) {
	if cmcLock == nil {
		cmcLock = new(sync.RWMutex)
	}

	dc, err := CreateOrUpdateDc(cmc, client)
	if err != nil {
		log.MyLogE("createDc Error:%v", err)
		return
	}

	var wsContainer *restful.Container = restful.NewContainer()
	mux := http.NewServeMux()
	handler := http.Handler(mux)

	if cmc.BaseAuthFile != "" {
		auth, err := newAuthenticatorFromBasicAuthFile(cmc.BaseAuthFile)
		if err != nil {
			log.MyLogE("Unable to StartPolicyHttpServer: %v", err)
			return
		}
		handler = WithAuthentication(mux, auth)
	}

	wsContainer.Router(restful.CurlyRouter{})
	wsContainer.ServeMux = mux
	ws1 := new(restful.WebService)
	ws1.Path("/ClusterManagerConfig").Consumes("*/*").Produces(restful.MIME_JSON)
	ws1.Route(ws1.GET("/").To(dc.ClusterManagerConfigGet).
		Doc("show all ClusterManagerConfig data").
		Writes(DynamicConfig{}))
	ws1.Route(ws1.GET("/set/{name}/{value}").To(dc.ClusterManagerConfigSet).
		Doc("set a ClusterManagerConfig data").
		Writes(ReturnMsg{}))

	wsContainer.Add(ws1)
	ClusterServer := &http.Server{
		Addr:    cmc.ConfigUrl,
		Handler: handler,
	}
	go func() {
		if cmc.ServerCertPath != "" && cmc.ServerKeyPath != "" { // for https
			glog.Fatal(ClusterServer.ListenAndServeTLS(cmc.ServerCertPath, cmc.ServerKeyPath))
		} else { //for http
			glog.Fatal(ClusterServer.ListenAndServe())
		}
	}()

}

// newAuthenticatorFromBasicAuthFile returns an authenticator.Request or an error
func newAuthenticatorFromBasicAuthFile(basicAuthFile string) (authenticator.Request, error) {
	basicAuthenticator, err := passwordfile.NewCSV(basicAuthFile)
	if err != nil {
		return nil, err
	}

	return basicauth.New(basicAuthenticator), nil
}

func WithAuthentication(handler http.Handler, auth authenticator.Request) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		_, ok, err := auth.AuthenticateRequest(req)
		if ok == false || err != nil {
			if err != nil {
				log.MyLogE("Unable to authenticate the request due to an error: %v", err)
			}
			genericapifilters.Unauthorized(true).ServeHTTP(w, req)
			return
		}
		handler.ServeHTTP(w, req)
	})
}

func restoreConfig(cmc *options.ClusterManagerConfig, client *clientset.Clientset) error {
	cm, _ := client.CoreV1().ConfigMaps(DynamicConfigMap_ns).Get(DynamicConfigMap_name, meta_v1.GetOptions{})
	if cm == nil {
		log.MyLogI("no config in store,no need to restore")
		return nil
	}
	data := cm.Data[DynamicConfig_itemname]
	res := &Item{}
	err := json.Unmarshal([]byte(data), &res)
	if err != nil {
		return fmt.Errorf("unexpected error")
	}
	log.MyLogI("start restore config")
	cmc.MaxCreateNode = res.MaxCreateNode
	cmc.CreateRequestCpuAbove = res.CreateRequestCpuAbove
	cmc.CreateLimitCpuAbove = res.CreateLimitCpuAbove
	cmc.CreateRequestMemAbove = res.CreateRequestMemAbove
	cmc.CreateLimitMemAbove = res.CreateLimitMemAbove
	cmc.DeleteRequestCpuUnder = res.DeleteRequestCpuUnder
	cmc.DeleteLimitCpuUnder = res.DeleteLimitCpuUnder
	cmc.DeleteRequestMemUnder = res.DeleteRequestMemUnder
	cmc.DeleteLimitMemUnder = res.DeleteLimitMemUnder
	if res.CreateMachineType == "" {
		curCreateMachineType = "ecs.xn4.small" // default create mahine type
	} else {
		curCreateMachineType = res.CreateMachineType
	}
	go func() {
		for {
			if err := UpdateCreateMachineType(cmc.ProviderUrl, "aliyun", curCreateMachineType); err != nil {
				log.MyLogE("UpdateCreateMachineType err:%v", err)
			} else {
				log.MyLogE("UpdateCreateMachineType aliyun create type %s success", curCreateMachineType)
				break
			}
			time.Sleep(3 * time.Second)
		}
	}()
	log.MyLogI("finish restore config")
	return nil

}
