package app

import (
	"encoding/json"
	"fmt"
	"time"
	"yunprovider/cmd/clustermanager/app/options"

	"yunprovider/common"
	"yunprovider/log"

	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	apiv1 "k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	provide_createnode_labelname  string = "io.enndata.yunprovider/createbyprovider"
	provide_createnode_labelvalue string = "true"
)

func createClient(s *options.ClusterManagerConfig) (*clientset.Clientset, error) {
	kubeconfig, err := clientcmd.BuildConfigFromFlags(s.APIMaster, s.Kubeconfig)
	if err != nil {
		return nil, fmt.Errorf("unable to build config from flags: %v", err)
	}

	kubeconfig.ContentType = s.ContentType

	cli, err := clientset.NewForConfig(restclient.AddUserAgent(kubeconfig, "enndata-clustermanager"))
	if err != nil {
		return nil, fmt.Errorf("invalid API configuration: %v", err)
	}
	return cli, nil
}

func Run(s *options.ClusterManagerConfig) error {
	if err := run(s); err != nil {
		return fmt.Errorf("failed to run ClusterManager : %v", err)

	}
	return nil
}

func checkConfig(s *options.ClusterManagerConfig) error {
	if s.Kubeconfig == "" && s.APIMaster == "" {
		return fmt.Errorf("kubeconfig or apimaster are empty")
	}
	if s.ProviderUrl == "" && s.NeedDeploy == true {
		return fmt.Errorf("providerurl is empty")
	}
	if s.CreateRequestCpuAbove >= 0 && s.DeleteRequestCpuUnder >= 0 && s.CreateRequestCpuAbove <= s.DeleteRequestCpuUnder {
		return fmt.Errorf("createrequestcpuabove should bigger than deleterequestcpuunder")
	}
	if s.CreateLimitCpuAbove >= 0 && s.DeleteLimitCpuUnder >= 0 && s.CreateLimitCpuAbove <= s.DeleteLimitCpuUnder {
		return fmt.Errorf("createlimitcpuabove should bigger than deletelimitcpuunder")
	}
	if s.CreateRequestMemAbove >= 0 && s.DeleteRequestMemUnder >= 0 && s.CreateRequestMemAbove <= s.DeleteRequestMemUnder {
		return fmt.Errorf("createrequestmemabove should bigger than deleterequestmemunder")
	}
	if s.CreateLimitMemAbove >= 0 && s.DeleteLimitMemUnder >= 0 && s.CreateLimitMemAbove <= s.DeleteLimitMemUnder {
		return fmt.Errorf("createlimitmemabove should bigger than deletelimitmemunder")
	}
	return nil
}

func waitProviderHealth(providerurl string) {
	for {
		if err := IsProviderHealth(providerurl); err == nil {
			log.MyLogI("provider url %s is health", providerurl)
			return
		} else {
			log.MyLogE("provider url %s is not health, err:%v", providerurl, err)
		}
		time.Sleep(1 * time.Second)
	}
}

func run(s *options.ClusterManagerConfig) (err error) {
	errCheck := checkConfig(s)
	if errCheck != nil {
		return errCheck
	}
	client, errc := createClient(s)
	if errc != nil {
		return errc
	}
	waitProviderHealth(s.ProviderUrl)
	nodeInfo, err := GetCreatedNodeInfo(s.ProviderUrl)
	if err != nil {
		return err
	}

	providerInfo, errPro := GetProviderInfo(s.ProviderUrl)
	if errPro != nil {
		return errPro
	}

	mm := NewMachineManager(nodeInfo.MaxNodeIndex+1, s.MaxCreateNode, s.NeedDeploy, s.ProviderUrl, s.DeployUrl,
		providerInfo.CreateMachinePassword, client, 120*time.Second)

	restoreConfig(s, client)
	if s.ConfigUrl != "" {
		go StartHttpServer(s, client)
	}

	for {
		err := doCheck2(s, client, mm)
		if err != nil {
			log.MyLogE("doCheck err:%v\n", err)
		}
		time.Sleep(s.ClusterCheckInterval)
	}
	return nil
}

func doCheck2(s *options.ClusterManagerConfig, client *clientset.Clientset, mm *MachineManager) error {
	log.MyLogI("")
	if errRecycle := DoMachineRecycle(s.ProviderUrl, s.DeployUrl, s.NeedDeploy, client, s.MachineDeployTimeout, s.MachineRestartInterval, s.MachinePassword, mm); errRecycle != nil {
		log.MyLogE("DoMachineRecyle err:%v\n", errRecycle)
	}
	mm.Check()
	info, err := GetClusterInf(client)
	if err != nil {
		return err
	}
	log.MyLogI("ClusterInfo TotalNode:%d, TotalCpu:%d CPU, TotalMem:%d GB, RequestCpuRatio:%.2f%%, RequestMemRatio:%.2f%%", info.TotalNode,
		info.TotalCpu/1000, info.TotalMem/1024/1024/1024, info.RequestCpuRatio, info.RequestMemRatio)
	//buf, _ := json.MarshalIndent(&info, " ", "  ")
	//log.MyLogI("%s\n\n", string(buf))

	nodeInfo, errNodeInfo := GetCreatedNodeInfo(s.ProviderUrl)
	if errNodeInfo != nil {
		return errNodeInfo
	}
	log.MyLogI("CreatedMachine TotalCreatedNode:%d, TotalCreatedCpu: %d CPU, TotalCreatedMem: %d GB, RunningNode: %d",
		nodeInfo.TotalCreatedNode, nodeInfo.TotalCreatedCpu, nodeInfo.TotalCreatedMem/1024, nodeInfo.RunningNode)
	//bufNodeInfo, _ := json.MarshalIndent(&nodeInfo, " ", "  ")
	//log.MyLogI("%s\n\n", string(bufNodeInfo))

	providerInfo, errPro := GetProviderInfo(s.ProviderUrl)
	if errPro != nil {
		return errPro
	}

	if machineSpec, exist := common.MachineSpec[providerInfo.CreateMachineType]; exist == false {
		return fmt.Errorf("Unknow providerinfo.machinetype %s", providerInfo.CreateMachineType)
	} else {
		num := getShouldCreateNodeNum(s, &info, nodeInfo, machineSpec)
		log.MyLogI("providerinfo: %d cpu, %dGB mem, createnum:%d MaxCanCreateNode:%d", machineSpec.NumCpu, machineSpec.MemGB, num, s.MaxCreateNode)
		mm.CreateMachine(num)
		if num == 0 {
			if checkShouldDeleteNode(s, &info) == true {
				return deleteNode(s.ProviderUrl, client)
			}
			log.MyLogI("Cluster is Ok")
		} else if num < 0 {
			return deleteNode(s.ProviderUrl, client)
		}
	}

	return nil
}

/*
func doCheck(s *options.ClusterManagerConfig, client *clientset.Clientset) error {
	if errRecycle := DoMachineRecycle(s.ProviderUrl, s.DeployUrl, s.NeedDeploy, client, s.MachineDeployTimeout, s.MachinePassword); errRecycle != nil {
		log.MyLogE("DoMachineRecyle err:%v\n", errRecycle)
	}

	info, err := GetClusterInf(client)
	if err != nil {
		return err
	}
	buf, _ := json.MarshalIndent(&info, " ", "  ")
	log.MyLogI("%s\n\n", string(buf))

	if checkShouldCreateNode(s, &info) == true {
		return createNode(client, s)
	}
	if checkShouldDeleteNode(s, &info) == true {
		return deleteNode(s.ProviderUrl, client)
	}
	log.MyLogI("doCheck cluster is ok\n")
	return nil
}*/

func createNode(client *clientset.Clientset, s *options.ClusterManagerConfig) error {
	log.MyLogI("start createNode\n")

	nodeInfo, err := GetCreatedNodeInfo(s.ProviderUrl)
	if err != nil {
		return err
	}
	buf, _ := json.MarshalIndent(&nodeInfo, " ", "  ")
	log.MyLogI("%s\n\n", string(buf))

	if nodeInfo.TotalCreatedNode >= s.MaxCreateNode {
		log.MyLogE("total create node = %d > MaxCreateNode = %d return\n", nodeInfo.TotalCreatedNode, s.MaxCreateNode)
		return nil
	}

	machineName, hostname := getMachineNameAndHostnameByIndex(nodeInfo.MaxNodeIndex + 1)
	_, errCreate := CreateMachine(s.ProviderUrl, machineName, hostname)
	if errCreate != nil {
		return errCreate
	} else {
		log.MyLogI("Create machine %s success", machineName)
	}

	machine, errStart := StartMachine(s.ProviderUrl, machineName)
	if errStart != nil {
		return errStart
	} else {
		log.MyLogI("start machine %s success", machineName)
	}

	if s.NeedDeploy == true {
		log.MyLogI("Start Sleep 20 seconds to wait machine ready")
		time.Sleep(20 * time.Second) // sleep 10s to wait machine ok
		log.MyLogI("Start deploy machine %s", machine.InnerIP)
		errDeploy := DeployMachine(s.DeployUrl, machine.InnerIP, s.MachinePassword)
		if errDeploy != nil {
			return errDeploy
		} else {
			log.MyLogI("deploy machine %s success", machineName)
		}
	} else {
		log.MyLogI("Start wait machine %s join cluster", machine.InnerIP)
		errwait := WaitNodeCreate(client, machine.InnerIP, 60*time.Second, nil)
		if errwait != nil {
			log.MyLogE("wait machine %s join cluster err:%v", machine.InnerIP, errwait)
		}
	}

	return LabelNode(client, machine.InnerIP, machine.Name, provide_createnode_labelname, provide_createnode_labelvalue)
}

func deleteNode(providerurl string, client *clientset.Clientset) error {
	log.MyLogI("start deleteNode\n")

	nodePodsMap := make(map[string]int)
	listPods, errPods := client.CoreV1().Pods(apiv1.NamespaceAll).List(meta_v1.ListOptions{})
	if errPods != nil {
		return errPods
	}
	for i := range listPods.Items {
		if isPodReady(&listPods.Items[i]) {
			curNum := nodePodsMap[listPods.Items[i].Spec.NodeName]
			nodePodsMap[listPods.Items[i].Spec.NodeName] = curNum + 1
		}
	}

	machines, errGet := GetCreatedMachine(providerurl)
	if errGet != nil {
		return errGet
	}
	deleteMachineIndex := -1
	minPods := 100000
	for i, m := range machines {
		num := nodePodsMap[m.InnerIP]
		if num < minPods {
			minPods = num
			deleteMachineIndex = i
		}
	}

	if deleteMachineIndex >= 0 {
		m := machines[deleteMachineIndex]
		log.MyLogI("start delete machine:%s", m.InnerIP)
		errRecycle := RecycleMachine(providerurl, m, 0, 5, time.Second)
		if errRecycle != nil {
			return errRecycle
		} else {
			log.MyLogI("recycle machine[%s] success", m.Name)
		}
		errDelete := DeleteNode(client, m.InnerIP, m.HostName)
		if errDelete != nil {
			return errDelete
		}
		log.MyLogI("machine[%s] is deleted\n", m.InnerIP)
	} else {
		return fmt.Errorf("no machine can be deleted\n")
	}
	return nil
}

func getShouldCreateNodeNum(s *options.ClusterManagerConfig, info *ClusterInfo, nodeinfo CreatedNodeInfo, spec common.Spec) int {
	maxNum := s.MaxCreateNode - int(info.ProviderNodeNum)
	reCalcFun := func(num int) (RequestCpuRatio, LimitCpuRatio, RequestMemRatio, LimitMemRatio float64) {
		totalCpu := info.TotalCpu + int64(num*spec.NumCpu*1000)
		totalMem := info.TotalMem + int64(num*spec.MemGB*1024*1024*1024)
		return float64(info.RequestedCpu*100) / float64(totalCpu), float64(info.LimitedCpu*100) / float64(totalCpu),
			float64(info.RequestedMem*100) / float64(totalMem), float64(info.LimitedMem*100) / float64(totalMem)
	}
	for i := 0; i < maxNum; i++ {
		RequestCpuRatio, LimitCpuRatio, RequestMemRatio, LimitMemRatio := reCalcFun(i)
		if s.CreateRequestCpuAbove >= 0 && s.CreateRequestCpuAbove <= RequestCpuRatio {
			continue
		}
		if s.CreateLimitCpuAbove >= 0 && s.CreateLimitCpuAbove <= LimitCpuRatio {
			continue
		}
		if s.CreateRequestMemAbove >= 0 && s.CreateRequestMemAbove <= RequestMemRatio {
			continue
		}
		if s.CreateLimitMemAbove >= 0 && s.CreateLimitMemAbove <= LimitMemRatio {
			continue
		}
		return i
	}
	return maxNum
}

func checkShouldCreateNode(s *options.ClusterManagerConfig, info *ClusterInfo) bool {
	if s.CreateRequestCpuAbove >= 0 && info.RequestCpuRatio >= s.CreateRequestCpuAbove {
		return true
	}
	if s.CreateLimitCpuAbove >= 0 && info.LimitCpuRatio >= s.CreateLimitCpuAbove {
		return true
	}
	if s.CreateRequestMemAbove >= 0 && info.RequestMemRatio >= s.CreateRequestMemAbove {
		return true
	}
	if s.CreateLimitMemAbove >= 0 && info.LimitMemRatio >= s.CreateLimitMemAbove {
		return true
	}
	return false
}

var isLastCheckShouldDeleteNode bool = false
var shouldDeleteNodeLastCheckTime time.Time

func checkShouldDeleteNode(s *options.ClusterManagerConfig, info *ClusterInfo) bool {
	set := func(v bool) {
		if v == false {
			isLastCheckShouldDeleteNode = false
		} else {
			if isLastCheckShouldDeleteNode == false {
				shouldDeleteNodeLastCheckTime = time.Now()
				isLastCheckShouldDeleteNode = true
			}
		}
	}
	check := func() bool {
		if isLastCheckShouldDeleteNode == true {
			if time.Since(shouldDeleteNodeLastCheckTime) >= s.DeletePaddingDuration {
				return true
			} else {
				log.MyLogI("checkShouldDeleteNode wait %v/%v", time.Since(shouldDeleteNodeLastCheckTime), s.DeletePaddingDuration)
			}
		}
		return false
	}

	if s.DeleteRequestCpuUnder >= 0 && info.RequestCpuRatio <= s.DeleteRequestCpuUnder {
		set(true)
		return check()
	}
	if s.DeleteLimitCpuUnder >= 0 && info.LimitCpuRatio <= s.DeleteLimitCpuUnder {
		set(true)
		return check()
	}
	if s.DeleteRequestMemUnder >= 0 && info.RequestMemRatio <= s.DeleteRequestMemUnder {
		set(true)
		return check()
	}
	if s.DeleteLimitMemUnder >= 0 && info.LimitMemRatio <= s.DeleteLimitMemUnder {
		set(true)
		return check()
	}

	nodeInfo, err := GetCreatedNodeInfo(s.ProviderUrl)
	if err != nil {
		log.MyLogE("GetCreatedNodeInfo err:%v", err)
		return false
	}

	if nodeInfo.TotalCreatedNode > s.MaxCreateNode { // this case should not wait padding time
		log.MyLogI("total create node = %d > MaxCreateNode = %d", nodeInfo.TotalCreatedNode, s.MaxCreateNode)
		return true
	}
	set(false)
	return false
}

var machineRestartTimeMap map[string]time.Time = make(map[string]time.Time)

func DoMachineRecycle(providerurl, deployurl string, NeedDeploy bool, client *clientset.Clientset,
	recycleTimeOut, restartinterval time.Duration, password string, mm *MachineManager) error {
	nodeStatusMap := make(map[string]bool)
	listNode, errNode := client.CoreV1().Nodes().List(meta_v1.ListOptions{})
	if errNode != nil {
		return errNode
	}
	for i := range listNode.Items {
		if isNodeReady(&listNode.Items[i]) {
			nodeStatusMap[listNode.Items[i].Name] = true
		} else {
			nodeStatusMap[listNode.Items[i].Name] = false
		}
	}

	machines, errGet := GetCreatedMachine(providerurl)
	if errGet != nil {
		return errGet
	}

	// recycle machines
	machinesStatusMap := make(map[string]bool)
	creatingMachine := make(map[string]bool)
	for _, m := range machines {
		if mm.IsMachineCreating(m.Name) == true {
			creatingMachine[m.InnerIP] = true
			continue
		}
		ready, exist := nodeStatusMap[m.InnerIP]
		if exist == false { // the machine is not in cluster
			if m.Status == common.MachineStopped {
				log.MyLogI("machine %s is not in cluster and status is stopped start delete machine\n", m.InnerIP)
				errDelete := DeleteMachine(providerurl, m.Name)
				if errDelete != nil {
					log.MyLogE("delete machine %s err:%v\n", m.InnerIP, errDelete)
				}
				continue
			}
			if time.Since(m.CreationTime) > recycleTimeOut { // machine join cluster timeout
				errRecycle := RecycleMachine(providerurl, m, 0, 3, time.Second)
				if errRecycle != nil {
					return errRecycle
				}
			} else {
				if t, exist := machineRestartTimeMap[m.InnerIP]; exist == false || time.Since(t) > restartinterval {
					if NeedDeploy {
						log.MyLogI("machine %s:%s is not in cluster start deploy again\n", m.Name, m.InnerIP)
						errDeploy := DeployMachine(deployurl, m.InnerIP, password)
						if errDeploy != nil {
							log.MyLogE("deploy %s err:%v\n", m.InnerIP, errDeploy)
						} else {
							errLable := LabelNode(client, m.InnerIP, m.HostName, provide_createnode_labelname, provide_createnode_labelvalue)
							if errLable != nil {
								log.MyLogE("labelnode %s err:%v\n", m.InnerIP, errLable)
							}
						}
					} else {
						log.MyLogI("machine %s:%s is not in cluster restart machine\n", m.Name, m.InnerIP)
						errReboot := RebootMachine(providerurl, m)
						if errReboot != nil {
							log.MyLogE("restart %s err:%v\n", m.InnerIP, errReboot)
						} else {
							errLable := LabelNode(client, m.InnerIP, m.HostName, provide_createnode_labelname, provide_createnode_labelvalue)
							if errLable != nil {
								log.MyLogE("labelnode %s err:%v\n", m.InnerIP, errLable)
							}
						}
					}
					machineRestartTimeMap[m.InnerIP] = time.Now()
				}
			}
		} else if ready == false { // the machine is in cluster, but is not ready
			if t, exist := machineRestartTimeMap[m.InnerIP]; exist == false || time.Since(t) > restartinterval {
				log.MyLogI("node %s is not ready start reboot machine", m.InnerIP)
				errReboot := RebootMachine(providerurl, m)
				if errReboot != nil {
					return errReboot
				} else {
					log.MyLogI("reboot machine %s success", m.InnerIP)
				}
				machineRestartTimeMap[m.InnerIP] = time.Now()
			}
		}
		machinesStatusMap[m.InnerIP] = (m.Status == common.MachineRunning)
	}

	// recycle nodes
	for i := range listNode.Items {
		if isNodeReady(&listNode.Items[i]) == false && IsNodeLabel(&listNode.Items[i], provide_createnode_labelname, provide_createnode_labelvalue) {
			if _, exist := machinesStatusMap[listNode.Items[i].Name]; exist == false && creatingMachine[listNode.Items[i].Name] == false { // machine corresponding to node is not exist
				errDelete := DeleteNode(client, listNode.Items[i].Name, "")
				if errDelete != nil {
					return errDelete
				}
				log.MyLogI("Node[%s] is not ready and machine is not exist so recycled\n", listNode.Items[i].Name)
			} else {
				if IsNodeLabel(&listNode.Items[i], provide_createnode_labelname, provide_createnode_labelvalue) == false {
					LabelNode(client, listNode.Items[i].Name, "", provide_createnode_labelname, provide_createnode_labelvalue)
				}
			}
		}
	}
	return nil
}
