package main

import (
	"fmt"
	"os"
	"yunprovider/cmd/deployment/app"
	"yunprovider/cmd/deployment/app/options"
	"yunprovider/version"

	"github.com/golang/glog"
	"github.com/spf13/pflag"
)

func main() {
	deployment := options.NewDeploymentConfig()
	deployment.AddFlags(pflag.CommandLine)

	options.InitFlags()
	defer glog.Flush()

	version.PrintAndExitIfRequested()
	if err := app.Run(deployment); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
