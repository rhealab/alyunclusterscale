package pkg

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"net/http"
	"sync"
	"time"
	"yunprovider/aliyun"
	"yunprovider/cmd/cloudprovider/app/options"
	"yunprovider/common"
	"yunprovider/log"
	"yunprovider/metrics"

	alicommon "github.com/denverdino/aliyungo/common"
	"github.com/denverdino/aliyungo/ecs"
	"github.com/emicklei/go-restful"
	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus"
)

var wsContainer *restful.Container = restful.NewContainer()
var installOne sync.Once

func NewProvider(p *options.PublicClouldProvider) common.YunProvider {
	switch p.Type {
	case options.AliYun:
		return aliyun.NewProviderWithRegions(p.AliYunConfig.AliYun_AccessKeyID, p.AliYunConfig.AliYun_AccessKeySecret,
			p.AliYunConfig.AliYun_Regions, false)
	default:
		return nil
	}
}

type httpServer struct {
	provider common.YunProvider
	config   *options.PublicClouldProvider
}

func writeResponse(w io.Writer, err error, msgOk, msgFail string, object interface{}) {
	var retCode int
	var retErrStr string
	var objStr string
	var msg string
	if err != nil {
		retCode = 201
		retErrStr = err.Error()
		msg = msgFail
	} else {
		retCode = 200
		retErrStr = ""
		msg = msgOk
	}
	if object != nil && err == nil {
		buf, _ := json.MarshalIndent(object, " ", "  ")
		objStr = string(buf)

	}

	tmp := fmt.Sprintf("{\"code\":%d,\"msg\":\"%s\",\"err\":\"%s\", \"data\":\"%s\"}",
		retCode, msg, retErrStr, strings.Replace(objStr, "\"", "\\\"", -1))
	w.Write([]byte(tmp))
}

func (hs *httpServer) listMachines(request *restful.Request, response *restful.Response) {
	ms, err := hs.provider.GetAllMachines()
	writeResponse(response, err, "ok", "fail", ms)
}
func (hs *httpServer) findMachine(request *restful.Request, response *restful.Response) {
	id := request.PathParameter("machine-name")
	m, err := hs.provider.GetMachineByName(id)
	writeResponse(response, err, "ok", "fail", m)
	log.MyLogI("%s getMachines %s", request.Request.Host, id)
}

func (hs *httpServer) CreateMachine(request *restful.Request, response *restful.Response) {
	if hs.config.AllowHttpManageMachine == false {
		response.Write([]byte("create machine is not allowed"))
		return
	}
	machinename := request.PathParameter("machine-name")
	hostname := request.PathParameter("host-name")

	m, err := CreateMachine(hs.provider, hs.config, machinename, hostname, true)
	writeResponse(response, err, "create sucess", "create fail "+m.Name, m)
	log.MyLogI("%s createMachines %s, %v", request.Request.Host, machinename, err)

}

func (hs *httpServer) UpdateCreatMachineType(request *restful.Request, response *restful.Response) {
	providername := request.PathParameter("providername")
	machinetype := request.PathParameter("machinetype")
	switch providername {
	case "aliyun":
		if common.IsMachineTypeExist(machinetype) == false {
			log.MyLogE("prividertype %s type %s is not supported", providername, machinetype)
			writeResponse(response, fmt.Errorf("prividertype %s type %s is not supported", providername, machinetype), "", "set fail", nil)
			return
		} else {
			hs.config.AliYun_InstanceType = machinetype
			log.MyLogI("UpdateCreatMachineType aliyun create mahinetype %s", machinetype)
		}
	default:
		log.MyLogE("prividertype %s is not supported", providername)
		writeResponse(response, fmt.Errorf("prividertype %s is not supported", providername), "", "set fail", nil)
		return
	}
	writeResponse(response, nil, fmt.Sprintf("set %s %s success", providername, machinetype), "", nil)
}

func (hs *httpServer) StartMachine(request *restful.Request, response *restful.Response) {
	if hs.config.AllowHttpManageMachine == false {
		response.Write([]byte("create machine is not allowed"))
		return
	}
	machinename := request.PathParameter("machine-name")
	err := StartMachine(hs.provider, machinename)
	if err != nil {
		writeResponse(response, err, "start sucess", "start fail", nil)
	}
	m, errGet := hs.provider.GetMachineByName(machinename)
	writeResponse(response, errGet, "start sucess", "start fail", m)
	log.MyLogI("%s startMachines %s", request.Request.Host, machinename)
}

func (hs *httpServer) RestartMachine(request *restful.Request, response *restful.Response) {
	if hs.config.AllowHttpManageMachine == false {
		response.Write([]byte("create machine is not allowed"))
		return
	}
	machinename := request.PathParameter("machine-name")
	err := RestartMachine(hs.provider, machinename)
	writeResponse(response, err, "restart sucess", "restart fail", nil)
	log.MyLogI("%s restartMachines %s, %v", request.Request.Host, machinename, err)
}

func (hs *httpServer) StopMachine(request *restful.Request, response *restful.Response) {
	if hs.config.AllowHttpManageMachine == false {
		response.Write([]byte("create machine is not allowed"))
		return
	}
	machinename := request.PathParameter("machine-name")
	err := StopMachine(hs.provider, machinename)
	writeResponse(response, err, "stop sucess", "stop fail", nil)
	log.MyLogI("%s stopMachines %s, %v", request.Request.Host, machinename, err)
}
func (hs *httpServer) ChangeMachineHostname(request *restful.Request, response *restful.Response) {
	if hs.config.AllowHttpManageMachine == false {
		response.Write([]byte("create machine is not allowed"))
		return
	}
	machinename := request.PathParameter("machine-name")
	hostname := request.PathParameter("host-name")
	err := ChangeHostName(hs.provider, machinename, hostname)
	writeResponse(response, err, "change sucess", "change fail", nil)
	log.MyLogI("%s changehostname %s, %s, %v", request.Request.Host, machinename, hostname, err)
}
func (hs *httpServer) DeleteMachine(request *restful.Request, response *restful.Response) {
	if hs.config.AllowHttpManageMachine == false {
		response.Write([]byte("create machine is not allowed"))
		return
	}
	machinename := request.PathParameter("machine-name")
	err := DeleteMachine(hs.provider, machinename)
	writeResponse(response, err, "delete sucess", "delete fail", nil)
	log.MyLogI("%s deletemachine %s, %v", request.Request.Host, machinename, err)
}

func (hs *httpServer) GetProviderInfo(request *restful.Request, response *restful.Response) {
	info := common.ProviderInfo{
		CreateMachinePassword: hs.config.AliYun_InstanceDefaultPassword,
		CreateMachineType:     hs.config.AliYun_InstanceType,
	}
	buf, _ := json.Marshal(&info)
	response.Write(buf)
}

func Health(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}

func installHttpServer(provider common.YunProvider, config *options.PublicClouldProvider) {
	mux := http.NewServeMux()
	mux.Handle("/metrics", prometheus.Handler())
	mux.HandleFunc("/health", Health)
	wsContainer.Router(restful.CurlyRouter{})
	wsContainer.ServeMux = mux
	httpServer := &httpServer{provider: provider, config: config}

	// list all machines
	ws1 := new(restful.WebService)
	ws1.Path("/machines").Consumes("*/*").Produces(restful.MIME_JSON)
	ws1.Route(ws1.GET("/{machine-name}").To(httpServer.findMachine).
		Doc("show machine info").
		Param(ws1.PathParameter("machine-name", "the name of machine").DataType("string")).
		Writes(common.YunMachine{}))
	ws1.Route(ws1.GET("/start/{machine-name}").To(httpServer.StartMachine).
		Doc("start a machine").
		Param(ws1.PathParameter("machine-name", "the name of the machine to start").DataType("string")).
		Writes(common.YunMachine{}))
	ws1.Route(ws1.GET("/stop/{machine-name}").To(httpServer.StopMachine).
		Doc("stop a machine").
		Param(ws1.PathParameter("machine-name", "the name of the machine to stop").DataType("string")).
		Writes(common.YunMachine{}))
	ws1.Route(ws1.GET("/delete/{machine-name}").To(httpServer.DeleteMachine).
		Doc("delete a machine").
		Param(ws1.PathParameter("machine-name", "the name of the machine to delete").DataType("string")).
		Writes(common.YunMachine{}))
	ws1.Route(ws1.GET("/create/{machine-name}/{host-name}").To(httpServer.CreateMachine).
		Doc("create a new machine").
		Param(ws1.PathParameter("machine-name", "the name of the machine to create").DataType("string")).
		Param(ws1.PathParameter("host-name", "the hostname of the machine to create").DataType("string")).
		Writes(common.YunMachine{}))
	ws1.Route(ws1.GET("/hostname/{machine-name}/{host-name}").To(httpServer.ChangeMachineHostname).
		Doc("change machine hostname").
		Param(ws1.PathParameter("machine-name", "the name of the machine to change").DataType("string")).
		Param(ws1.PathParameter("host-name", "the new hostname of the machine to change").DataType("string")).
		Writes(common.YunMachine{}))
	ws1.Route(ws1.GET("/info").To(httpServer.GetProviderInfo).
		Doc("get provider create machine info").
		Writes(common.ProviderInfo{}))
	ws1.Route(ws1.GET("/").To(httpServer.listMachines).
		Doc("show machine infos").
		Writes([]common.YunMachine{}))

	// create machine
	ws2 := new(restful.WebService)
	ws2.Path("/create").Consumes("*/*").Produces(restful.MIME_JSON)
	ws2.Route(ws2.POST("/{machine-name}/{host-name}").To(httpServer.CreateMachine).
		Doc("create a new machine").
		Param(ws2.PathParameter("machine-name", "the name of the machine to create").DataType("string")).
		Param(ws2.PathParameter("host-name", "the hostname of the machine to create").DataType("string")).
		Writes(common.YunMachine{}))
	ws2.Route(ws2.GET("/{machine-name}/{host-name}").To(httpServer.CreateMachine).
		Doc("create a new machine").
		Param(ws2.PathParameter("machine-name", "the name of the machine to create").DataType("string")).
		Param(ws2.PathParameter("host-name", "the hostname of the machine to create").DataType("string")).
		Writes(common.YunMachine{}))

	// start machine
	ws3 := new(restful.WebService)
	ws3.Path("/start").Consumes("*/*").Produces(restful.MIME_JSON)
	ws3.Route(ws3.GET("/{machine-name}").To(httpServer.StartMachine).
		Doc("start a machine").
		Param(ws3.PathParameter("machine-name", "the name of the machine to start").DataType("string")).
		Writes(common.YunMachine{}))

	// restart machine
	ws4 := new(restful.WebService)
	ws4.Path("/restart").Consumes("*/*").Produces(restful.MIME_JSON)
	ws4.Route(ws4.GET("/{machine-name}").To(httpServer.RestartMachine).
		Doc("restart a machine").
		Param(ws4.PathParameter("machine-name", "the name of the machine to restart").DataType("string")).
		Writes(common.YunMachine{}))

	// stop machine
	ws5 := new(restful.WebService)
	ws5.Path("/stop").Consumes("*/*").Produces(restful.MIME_JSON)
	ws5.Route(ws5.GET("/{machine-name}").To(httpServer.StopMachine).
		Doc("stop a machine").
		Param(ws5.PathParameter("machine-name", "the name of the machine to stop").DataType("string")).
		Writes(common.YunMachine{}))

	// delete machine
	ws6 := new(restful.WebService)
	ws6.Path("/delete").Consumes("*/*").Produces(restful.MIME_JSON)
	ws6.Route(ws6.GET("/{machine-name}").To(httpServer.DeleteMachine).
		Doc("delete a machine").
		Param(ws6.PathParameter("machine-name", "the name of the machine to delete").DataType("string")).
		Writes(common.YunMachine{}))

	// update create machine type UpdateCreatMachineType
	ws7 := new(restful.WebService)
	ws7.Path("/setcreatemachinetype").Consumes("*/*").Produces(restful.MIME_JSON)
	ws7.Route(ws7.GET("/{providername}/{machinetype}").To(httpServer.UpdateCreatMachineType).
		Doc("set next create machine type").
		Param(ws7.PathParameter("providername", "the provider name of the machine created by").DataType("string")).
		Param(ws7.PathParameter("machinetype", "the machine type created by").DataType("string")).
		Writes(common.YunMachine{}))

	wsContainer.Add(ws1)
	wsContainer.Add(ws2)
	wsContainer.Add(ws3)
	wsContainer.Add(ws4)
	wsContainer.Add(ws5)
	wsContainer.Add(ws6)
	wsContainer.Add(ws7)
	server := &http.Server{Addr: config.HttpServerAddr, Handler: wsContainer}
	glog.Fatal(server.ListenAndServe())
}

func CreateMachine(provider common.YunProvider, config *options.PublicClouldProvider,
	instanceName, hostName string, wait bool) (common.YunMachine, error) {

	_, err := provider.GetMachineByName(instanceName)
	if err == nil { // machine is exist
		return common.YunMachine{}, fmt.Errorf("machine %s is exist", instanceName)
	}

	switch config.Type {
	case options.AliYun:
		optFun := func() interface{} {
			dataDisk := make([]ecs.DataDiskType, 0)
			for i := 0; i < config.AliYun_InstanceDataDiskNum; i++ {
				dataDisk = append(dataDisk, ecs.DataDiskType{
					Size:               config.AliYun_InstanceDataDiskSize,
					Category:           ecs.DiskCategory(config.AliYun_InstanceDataDiskCategory),
					DiskName:           fmt.Sprintf("%s_datadisk_%d", hostName, i),
					Description:        "datadisk create by yunprovider",
					DeleteWithInstance: config.AliYun_InstanceDataRemoveWhenVMDelete,
				})
			}
			return aliyun.CreateOption{
				ecs.CreateInstanceArgs{
					RegionId:                alicommon.Region(config.AliYun_Regions[0]),
					ImageId:                 config.AliYun_CreateImageId,
					InstanceType:            config.AliYun_InstanceType,
					SecurityGroupId:         config.AliYun_SecurityGroupId,
					InstanceName:            instanceName,
					Description:             "create by yunprovider",
					InternetChargeType:      alicommon.PayByTraffic,
					InternetMaxBandwidthIn:  config.AliYun_InternetMaxBandwidthIn,
					InternetMaxBandwidthOut: config.AliYun_InternetMaxBandwidthOut,
					HostName:                hostName,
					Password:                config.AliYun_InstanceDefaultPassword,
					IoOptimized:             ecs.IoOptimizedOptimized,
					SystemDisk: ecs.SystemDiskType{
						Size:        config.AliYun_InstanceSystemDiskSize,
						Category:    ecs.DiskCategory(config.AliYun_InstanceSystemDiskCategory),
						DiskName:    "systemdisk",
						Description: "systemdisk create by yunprovider",
					},
					DataDisk:           dataDisk,
					InstanceChargeType: alicommon.PostPaid,
				},
			}
		}
		var machine common.YunMachine
		var err error
		if wait == false {
			machine, err = provider.CreateMachine(optFun)
		} else {
			machine, err = provider.CreateMachineWait(optFun, -1)
		}
		if err == nil {
			metrics.CreatedMachineCount.Inc()
		}
		return machine, err
	default:
		return common.YunMachine{}, fmt.Errorf("unknow yun provider type")
	}
}

func StopMachine(provider common.YunProvider, machinename string) error {
	m, err := provider.GetMachineByName(machinename)
	if err != nil {
		return err
	}
	err = provider.StopMachineWait(&m, true, -1)
	if err != nil {
		return err
	}
	metrics.StopedMachineCount.Inc()
	return nil
}

func StartMachine(provider common.YunProvider, machinename string) error {
	m, err := provider.GetMachineByName(machinename)
	if err != nil {
		return err
	}
	err = provider.StartMachineWait(&m, -1)
	if err != nil {
		return err
	}
	metrics.StartMachineCount.Inc()
	return nil
}

func RestartMachine(provider common.YunProvider, machinename string) error {
	m, err := provider.GetMachineByName(machinename)
	if err != nil {
		return err
	}
	err = provider.RestartMachineWait(&m, true, -1)
	if err != nil {
		return err
	}
	metrics.RestartMachineCount.Inc()
	return nil
}

func DeleteMachine(provider common.YunProvider, machinename string) error {
	m, err := provider.GetMachineByName(machinename)
	if err != nil {
		return err
	}
	err = provider.DeleteMachine(&m)
	if err != nil {
		return err
	}
	metrics.DeletedMachineCount.Inc()
	return nil
}

func ChangeHostName(provider common.YunProvider, machinename, newHostname string) error {
	m, err := provider.GetMachineByName(machinename)
	if err != nil {
		return err
	}
	if m.HostName == newHostname {
		return nil
	}
	n := m
	n.HostName = newHostname
	err = provider.ModifyMachine(&m, &n)
	return err
}

func RunProvider(provider common.YunProvider, config *options.PublicClouldProvider) error {
	installOne.Do(func() { installHttpServer(provider, config) })
	for {
		glog.Info("running...\n")
		time.Sleep(1 * time.Second)
	}
	return nil
}
