package aliyun

import (
	"fmt"
	"reflect"
	"sync"
	"time"
	yunprovidercommon "yunprovider/common"

	"github.com/denverdino/aliyungo/common"
	"github.com/denverdino/aliyungo/ecs"
)

const (
	All_MachineName   = ""
	All_InstanceId    = ""
	All_PublicIP      = ""
	All_MachineStatus = ""
)

type CreateOption struct {
	ecs.CreateInstanceArgs
}

var NotFound error = fmt.Errorf("Not Found")
var TimeOut error = fmt.Errorf("TimeOut")
var defaultRegions []string = []string{string(common.Shanghai)}

type MachineCache struct {
	machine   yunprovidercommon.YunMachine
	cacheTime time.Time
}
type provider struct {
	ecsClient *ecs.Client

	machineCache []MachineCache
	machineTTL   time.Duration

	regions []string

	cacheEnable bool

	mu sync.Mutex
}

func NewProviderWithRegions(accessKeyId, accessSecret string, regions []string, withCache bool) yunprovidercommon.YunProvider {
	return newProvider(accessKeyId, accessSecret, regions, withCache)
}

func NewProvider(accessKeyId, accessSecret string, withCache bool) yunprovidercommon.YunProvider {
	return newProvider(accessKeyId, accessSecret, defaultRegions, withCache)
}

func newProvider(accessKeyId, accessSecret string, regions []string, withCache bool) yunprovidercommon.YunProvider {
	client := ecs.NewClient(accessKeyId, accessSecret)
	client.DescribeInstanceTypes()
	var ttl time.Duration
	if withCache {
		ttl = 10 * time.Second
	}
	return &provider{
		ecsClient:    client,
		machineCache: nil,
		machineTTL:   ttl,
		regions:      regions,
		cacheEnable:  true,
	}
}
func (p *provider) GetAllMachines() ([]yunprovidercommon.YunMachine, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.getAllMachines(All_MachineName, All_MachineStatus, All_PublicIP, All_InstanceId, false, true)
}

func (p *provider) GetRunningMachines() ([]yunprovidercommon.YunMachine, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.getAllMachines(All_MachineName, string(ecs.Running), All_PublicIP, All_InstanceId, false, true)
}

func (p *provider) GetMachineByName(name string) (yunprovidercommon.YunMachine, error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	ret, err := p.getAllMachines(name, All_MachineStatus, All_PublicIP, All_InstanceId, true, true)
	if err == nil && len(ret) > 0 {
		return ret[0], nil
	}
	return yunprovidercommon.YunMachine{}, NotFound
}
func (p *provider) GetMachineById(id string) (yunprovidercommon.YunMachine, error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	ret, err := p.getAllMachines(All_MachineName, All_MachineStatus, All_PublicIP, id, true, true)
	if err == nil && len(ret) > 0 {
		return ret[0], nil
	}
	return yunprovidercommon.YunMachine{}, NotFound
}

func (p *provider) GetMachineByPubliceIP(ip string) (yunprovidercommon.YunMachine, error) {
	p.mu.Lock()
	defer p.mu.Unlock()
	ret, err := p.getAllMachines(All_MachineName, All_MachineStatus, ip, All_InstanceId, true, true)
	if err == nil && len(ret) > 0 {
		return ret[0], nil
	}
	return yunprovidercommon.YunMachine{}, NotFound
}
func (p *provider) CreateMachineWait(opsFunc func() interface{}, timeOut int) (yunprovidercommon.YunMachine, error) {
	m, err := p.CreateMachine(opsFunc)
	if err != nil {
		return yunprovidercommon.YunMachine{}, err
	}
	errWait := p.waitMachine(&m, string(ecs.Stopped), timeOut)
	if errWait != nil {
		return yunprovidercommon.YunMachine{}, errWait
	} else {
		return p.GetMachineById(m.Id)
	}
}

func checkEqual(a, b interface{}, fieldName string) bool {
	va := reflect.ValueOf(a).Elem().FieldByName(fieldName)
	vb := reflect.ValueOf(b).Elem().FieldByName(fieldName)
	if va.Kind() != vb.Kind() {
		return false
	}
	switch va.Kind() {
	case reflect.Int:
		return va.Int() == vb.Int()
	case reflect.String:
		return va.String() == vb.String()
	case reflect.Struct:
		return true
	}
	return false
}
func checkModify(machine *yunprovidercommon.YunMachine, toMachine *yunprovidercommon.YunMachine) error {
	canModifyField := map[string]bool{"Name": true, "Description": true, "HostName": true}

	s := reflect.TypeOf(machine).Elem()
	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		if ok, _ := canModifyField[f.Name]; ok == false {
			if checkEqual(machine, toMachine, f.Name) == false {
				return fmt.Errorf("%s cann't be modified", f.Name)
			}
		}
	}
	return nil
}
func (p *provider) SetCacheEnable(v bool) {
	p.cacheEnable = v
}
func (p *provider) ModifyMachine(machine *yunprovidercommon.YunMachine, toMachine *yunprovidercommon.YunMachine) error {
	if err := checkModify(machine, toMachine); err != nil {
		return err
	}
	return p.modifyMachine(machine.Id, toMachine)
}

func (p *provider) CreateMachine(opsFunc func() interface{}) (yunprovidercommon.YunMachine, error) {
	id, err := p.createMachine(opsFunc)
	if err != nil {
		return yunprovidercommon.YunMachine{}, err
	}
	ret, errGet := p.getAllMachines(All_MachineName, All_MachineStatus, All_PublicIP, id, true, true)
	if errGet == nil && len(ret) > 0 {
		return ret[0], nil
	}
	return yunprovidercommon.YunMachine{}, NotFound
}

func (p *provider) RestartMachineWait(machine *yunprovidercommon.YunMachine, force bool, timeOut int) error {
	err := p.RestartMachine(machine, force)
	if err != nil {
		return err
	}
	return p.waitMachine(machine, string(ecs.Running), timeOut)
}

func (p *provider) RestartMachine(machine *yunprovidercommon.YunMachine, force bool) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.restartMachine(machine, force)
}

func (p *provider) StopMachine(machine *yunprovidercommon.YunMachine, force bool) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.stopMachine(machine, force)
}

func (p *provider) StartMachine(machine *yunprovidercommon.YunMachine) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.startMachine(machine)
}

func (p *provider) StartMachineWait(machine *yunprovidercommon.YunMachine, timeOut int) error {
	err := p.StartMachine(machine)
	if err != nil {
		return err
	}
	return p.waitMachine(machine, string(ecs.Running), timeOut)
}

func (p *provider) StopMachineWait(machine *yunprovidercommon.YunMachine, force bool, timeOut int) error {
	err := p.StopMachine(machine, force)
	if err != nil {
		return err
	}
	return p.waitMachine(machine, string(ecs.Stopped), timeOut)
}

func (p *provider) DeleteMachine(machine *yunprovidercommon.YunMachine) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	return p.deleteMachine(machine)
}

func (p *provider) getAllMachinesFromCache(name, status, publicIP, instanceId string, isOnlyOne bool) ([]yunprovidercommon.YunMachine, error) {
	now := time.Now()
	ret := make([]yunprovidercommon.YunMachine, 0, len(p.machineCache))
	for _, cacheMachine := range p.machineCache {
		if name != "" && cacheMachine.machine.Name != name {
			continue
		}
		if status != "" && string(cacheMachine.machine.Status) != status {
			continue
		}
		if publicIP != "" && cacheMachine.machine.PublicIP != publicIP {
			continue
		}
		if instanceId != "" && cacheMachine.machine.Id != instanceId {
			continue
		}
		if cacheMachine.cacheTime.Add(p.machineTTL).After(now) {
			if isOnlyOne {
				return []yunprovidercommon.YunMachine{cacheMachine.machine}, nil
			} else {
				ret = append(ret, cacheMachine.machine)
			}
		} else { // exceed the cache time limit
			return []yunprovidercommon.YunMachine{}, nil
		}
	}
	return ret, nil
}

func (p *provider) updateCache(newMachines []yunprovidercommon.YunMachine, updateTime time.Time) {

	for _, newMachine := range newMachines {
		var updated bool
		for i, cacheMachine := range p.machineCache {
			if newMachine.Id == cacheMachine.machine.Id &&
				newMachine.Name == cacheMachine.machine.Name {
				p.machineCache[i] = MachineCache{
					machine:   newMachine,
					cacheTime: updateTime,
				}
				updated = true
				break
			}
		}
		if updated == false {
			p.machineCache = append(p.machineCache, MachineCache{
				machine:   newMachine,
				cacheTime: updateTime,
			})
		}
	}

}

func (p *provider) modifyMachine(id string, toMachine *yunprovidercommon.YunMachine) error {
	args := &ecs.ModifyInstanceAttributeArgs{
		InstanceId:   toMachine.Id,
		InstanceName: toMachine.Name,
		Description:  toMachine.Description,
		HostName:     toMachine.HostName,
	}
	return p.ecsClient.ModifyInstanceAttribute(args)
}

func (p *provider) getAllMachines(name, status, publicIP, instanceId string, isOnlyOne, useCache bool) ([]yunprovidercommon.YunMachine, error) {
	var ret []yunprovidercommon.YunMachine
	if useCache && p.cacheEnable {
		var err error
		ret, err = p.getAllMachinesFromCache(name, status, publicIP, instanceId, isOnlyOne)
		if len(ret) > 0 && err == nil {
			return ret, nil
		}
	}
	ret = make([]yunprovidercommon.YunMachine, 0)

	for _, r := range p.regions {
		args := &ecs.DescribeInstancesArgs{
			RegionId:            common.Region(r),
			Status:              ecs.InstanceStatus(status),
			InstanceName:        name,
			InstanceIds:         strToJosn(instanceId),
			PublicIpAddresses:   strToJosn(publicIP),
			InstanceNetworkType: "Vpc",
		}
		if p.ecsClient == nil {
			return ret, fmt.Errorf("ecsClient is nil")
		}
		for {
			instances, result, err := p.ecsClient.DescribeInstances(args)
			if err != nil {
				return ret, fmt.Errorf("DescribeInstances[%v] err:%v", args, err)
			}
			for _, instance := range instances {
				ret = append(ret, yunprovidercommon.YunMachine{Name: instance.InstanceName,
					Id:           instance.InstanceId,
					HostName:     instance.HostName,
					PublicIP:     getIPFromSet(instance.PublicIpAddress),
					InnerIP:      getIPFromSet(instance.VpcAttributes.PrivateIpAddress),
					ImageId:      instance.ImageId,
					CreationTime: time.Time(instance.CreationTime),
					ExpiredTime:  time.Time(instance.ExpiredTime),
					Status:       yunprovidercommon.MachineStatus(instance.Status),
					CPU:          instance.CPU,
					Memory:       instance.Memory,
					Description:  instance.Description})
			}
			next := result.NextPage()
			if next != nil {
				args.Pagination = *next
			} else {
				break
			}
		}
		if isOnlyOne == true && len(ret) > 0 {
			p.updateCache(ret, time.Now())
			return ret, nil
		}
	}
	p.updateCache(ret, time.Now())

	return ret, nil
}

func getIPFromSet(ipset ecs.IpAddressSetType) string {
	if len(ipset.IpAddress) > 0 {
		return ipset.IpAddress[0]
	}
	return ""
}

func (p *provider) createMachine(opsFunc func() interface{}) (string, error) {
	obj := opsFunc()
	if opt, ok := obj.(CreateOption); ok {
		return p.ecsClient.CreateInstance(&opt.CreateInstanceArgs)
	} else {
		return "", fmt.Errorf("get option is not CreateOption")
	}
}

func (p *provider) restartMachine(machine *yunprovidercommon.YunMachine, force bool) error {
	return p.ecsClient.RebootInstance(machine.Id, force)
}

func (p *provider) stopMachine(machine *yunprovidercommon.YunMachine, force bool) error {
	return p.ecsClient.StopInstance(machine.Id, force)
}

func (p *provider) startMachine(machine *yunprovidercommon.YunMachine) error {
	return p.ecsClient.StartInstance(machine.Id)
}

func (p *provider) deleteMachine(machine *yunprovidercommon.YunMachine) error {
	return p.ecsClient.DeleteInstance(machine.Id)
}

func (p *provider) waitMachine(machine *yunprovidercommon.YunMachine, status string, timeOut int) error {
	if machine.Id == "" && machine.Name == "" {
		return fmt.Errorf("no machine is assinged")
	}
	funcGet := func() (yunprovidercommon.YunMachine, error) {
		if machine.Name != "" {
			return p.GetMachineByName(machine.Name)
		} else {
			return p.GetMachineById(machine.Id)
		}
	}
	timeOutCh := make(chan bool, 1)
	checkCh := make(chan bool, 1)
	if timeOut > 0 {
		go func() {
			time.Sleep(time.Duration(timeOut) * time.Second)
			timeOutCh <- true
		}()
	}
	go func() {
		for {
			time.Sleep(1 * time.Second)
			checkCh <- true
		}
	}()
	for {
		select {
		case <-timeOutCh:
			return TimeOut
		case <-checkCh:
			m, _ := funcGet()
			if string(m.Status) == status {
				return nil
			}
		}
	}
	return nil
}

func strToJosn(str string) string {
	if str == "" {
		return ""
	}
	return fmt.Sprintf("[\"%s\"]", str)
}
